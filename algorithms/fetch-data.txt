1.	Fetch services list from server.

2.	Accept service name as input and determine corresponding service id

3.	POST service id and device id to "/fetchToken" route to request a token.
	If response is negative print error message and exit else goto 4.

4.	Fetch data.
	If response is "Invalid token" then request new token from server and goto 5 else print data received and go back to menu.

5.	Fetch data.
	If response is "Invalid token" then print error message and exit.

