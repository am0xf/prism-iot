#!/usr/bin/env python3

from flask import Flask, request, jsonify

from datetime import datetime, timedelta

from Crypto.Random import get_random_bytes

app = Flask('Auth_Server')

token_validity_minutes = 1
issued_tokens = {} # {<token_string> : {'service id' : <str>, 'expiry' : <datetimestamp str>}}

@app.route('/verifyToken', methods=['POST'])
def verify_token():
	token = request.json # {'string' : <hex string> : {'service_id' : <str>, 'expiry' : <datetimestamp str>}
	resp = ''
	
	if token['string'] in issued_tokens and issued_tokens[token['string']]['service_id'] == token['service_id']:
		expiry = datetime.fromtimestamp(int(token['expiry']))
		if expiry > datetime.now():
			resp = 'Valid'
		else:
			resp = 'Invalid'
			del issued_tokens[token['string']]
	else:
		resp = 'Invalid'

	return resp

@app.route('/generateToken', methods=['POST'])
def generate_token():
	service_id = request.data.decode('utf-8')
	expiry = (datetime.now() + timedelta(minutes=token_validity_minutes)).strftime('%s')
	token_str = get_random_bytes(64).hex()

	token = {'service_id' : service_id, 'string' : token_str, 'expiry' : expiry}

	issued_tokens[token_str] = {'service_id' : service_id, 'expiry' : expiry}

	return jsonify(token)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=12346)

