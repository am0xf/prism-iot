#!/bin/bash

_term () {
	echo "Shutting down"

	kill -TERM $AUTH_PID $RSRC_PID
}

trap _term SIGTERM

mkdir logs

./auth-server.py &>> logs/auth-server &
AUTH_PID=$!

./rsrc-server.py &>> logs/rsrc-server &
RSRC_PID=$!

wait

