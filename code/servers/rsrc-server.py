#!/usr/bin/env python3

from flask import Flask, request

import requests

app = Flask('Resrc_Server')
auth_server = 'http://127.0.0.1:12346'
verify_token_url = auth_server + '/verifyToken'

@app.route('/getData', methods=['POST'])
def get_data():
	resp = requests.post(verify_token_url, json=request.get_json()).text

	if resp == 'Valid':
		return 'Here is the data'
	else:
		return 'Invalid token'

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=12347)

