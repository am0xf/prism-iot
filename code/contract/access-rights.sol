pragma solidity ^0.6.4;

contract access_rights 
{
	struct device { 
		bool privileged;
		mapping(uint8 => bool) rights;
	}

	mapping(uint64 => device) public devices;

	event ret_val(bool val);

	constructor(uint64 hub_id) public
	{
		devices[hub_id].privileged = true;
	}

	function access_permitted(uint64 target_id, uint8 service_id) public view returns (bool)
	{
		return devices[target_id].rights[service_id];
	}

	function modify_privilege(uint64 device_id, uint64 target_id, bool privileged) public
	{
		if (devices[device_id].privileged) { // requesting device is privileged
			devices[target_id].privileged = privileged;

			emit ret_val(true);
		}

		emit ret_val(false);
	}

	function modify_access_rights(uint64 device_id, uint64 target_id, uint8[] memory service_ids, bool[] memory modifications) public
	{
		if (devices[device_id].privileged) { // requesting device is privileged
			for (uint8 i = 0; i < service_ids.length; i++) {
				devices[target_id].rights[service_ids[i]] = modifications[i];
			}

			emit ret_val(true);
		}

		emit ret_val(false);
	}
}

