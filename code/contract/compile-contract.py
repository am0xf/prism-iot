#!/usr/bin/env python3

from solc import compile_standard

import json
import sys

filename = sys.argv[1]
contract_name = sys.argv[2]

compiled_contract = compile_standard( 
		{
			'language' : 'Solidity',

			'sources' : {
				filename : {
					'content' : open(filename).read().strip()
					}
				},

			'settings' : {
				'outputSelection' : {
					'*' : {
						'*' : ['metadata', 'evm.bytecode', 'evm.bytecode.sourceMap']
						}
					}
				}
			}
		)

bytecode = compiled_contract['contracts'][filename][contract_name]['evm']['bytecode']['object']
abi = json.dumps(json.loads(compiled_contract['contracts'][filename][contract_name]['metadata'])['output']['abi'])

open(filename.split('.')[0] + '.json', 'w').write(json.dumps({'bytecode' : bytecode, 'abi' : abi}))

