#!/usr/bin/env python3

import time

import requests
import json

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# device properties
device_id = ''
device_name = ''
wait_interval = 5 # interval to wait to check that hub is online. set from benchmarks on my ssd where hub takes around 3s to boot.

# caching service password
password = ''

# get ip dynamically
hub_url = 'http://hub.test:12345'

# urls on hub
register_url = hub_url + '/register'

check_priv_url = hub_url + '/checkPriv'
modify_rights_url = hub_url + '/modifyRights'
modify_priv_url = hub_url + '/modifyPriv'
fetch_token_url = hub_url + '/fetchToken'
remove_device_url = hub_url + '/removeDev'

get_devices_url = hub_url + '/getDevices'
get_services_url = hub_url + '/getServices'

def decrypt(ciphertext, tag, nonce):
	ciphertext = bytes.fromhex(ciphertext)
	tag = bytes.fromhex(tag) 
	nonce = bytes.fromhex(nonce)

	cipher_aes_dec = AES.new(password,AES.MODE_EAX, nonce)
		
	return cipher_aes_dec.decrypt_and_verify(ciphertext, tag).decode('utf-8')

def encrypt(plaintext):
	plaintext = plaintext.encode('utf-8')

	cipher_aes_enc = AES.new(password, AES.MODE_EAX)

	ciphertext, tag = cipher_aes_enc.encrypt_and_digest(plaintext)

	return (ciphertext.hex(), tag.hex(), cipher_aes_enc.nonce.hex())

def boot():
	global device_id, device_name, password

	# set device id
	try:
		device_id = open('device-id').read().strip()

	except FileNotFoundError:
		print('Setting random device ID')
		device_id = get_random_bytes(8).hex()
		open('device-id', 'w').write(device_id)

	# set device name
	try:
		device_name = open('device-name').read().strip()

	except FileNotFoundError:
		device_name = input('Enter a device name : ')
		open('device-name', 'w').write(device_name)

	# get token cache service password
	try:
		inp = input('New password ? (y/n) : ')

		if inp == 'y':
			raise FileNotFoundError # to reduce code duplication. using raise as a goto.

		else:
			password = open('enc-pswd', 'r').read().strip()

	except FileNotFoundError:
		password = input('Enter the token caching service password : ')
		open('enc-pswd', 'w').write(password)

	password = password.encode('utf-8')

	# register device

	if not register():
		print('Device Registration error. Exiting')
	
		return
			
	# start the ui
	ui()

def ui():
	print('Device name : ' + device_name)

	options = {
			'1' : 'Fetch data',
			'2' : 'Check privilege',
			'3' : 'Modify rights',
			'4' : 'Modify privilege',
			'5' : 'Remove device',
			'6' : 'Exit'
			}

	while True:
		for option in options:
			print(option, options[option])

		choice = input('> ')
		
		# check if hub is online, device is registered get session key
		if choice != '6':
			if not register():
				print('Device Registration error. Exiting')
				return
			
		if choice == '1':
			fetch_data()
		elif choice == '2':
			check_privileged()
		elif choice == '3':
			modify_rights()
		elif choice == '4':
			modify_privilege()
		elif choice == '5':
			remove_device()
		elif choice == '6':
			return
		else:
			print('Wrong choice')

def register():
	ciphertext, tag, nonce = encrypt(device_id)

	while True:
		print('Checking if server is online')

		try:
			resp = requests.post(register_url, json={
				'device_name' : device_name,
				'secret' : ciphertext,
				'tag' : tag,
				'nonce' : nonce
				}).json()

			if resp['status'] != 'Already registered':
				print(resp['status'])

			return True if resp['status'] in ['Registered', 'Already registered'] else False

		except:
			time.sleep(wait_interval) # wait for hub to come back online

def get_devices():
	rq = requests.get(get_devices_url).json()
	devices = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))
	return devices

def get_device_id(): 
	devices = get_devices()

	for dev_id in devices:
		print(devices[dev_id], dev_id)
	
	target = input('Enter device name : ')

	for dev_id in devices:
		if devices[dev_id] == target:
			return dev_id

def get_services():
	rq = requests.get(get_services_url).json()
	services = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))

	return services

def get_service_id():
	services = get_services()

	for service in services:
		print(services[service]['name'])

	while True:
		service_name = input('Enter service name to fetch data from (-1 to enter no service) : ').lower()

		if service_name == '-1':
			return '-1'

		for serv_id in services:
			if services[serv_id]['name'] == service_name:
				return serv_id
	
def check_privileged():
	rq = {}
	rq['secret'], rq['tag'], rq['nonce'] = encrypt(json.dumps({
		'device_id' : device_id
		}))

	rsp = requests.post(check_priv_url, json=rq).json()
	resp = json.loads(decrypt(rsp['secret'], rsp['tag'], rsp['nonce']))

	print(resp['status'])

def remove_device():
	target_id = get_device_id()

	rq = {}
	rq['secret'], rq['tag'], rq['nonce'] = encrypt(json.dumps({
		'device_id' : device_id,
		'target_id' : target_id
		}))

	rsp = requests.post(remove_device_url, json=rq).json()
	resp = json.loads(decrypt(rsp['secret'], rsp['tag'], rsp['nonce']))

	print(resp['status'])

def modify_privilege():
	target_id = get_device_id()

	action = input('Add or remove privilege (a/r) ? ')

	rq = {}
	rq['secret'], rq['tag'], rq['nonce'] = encrypt(json.dumps({
		'device_id' : device_id,
		'target_id' : target_id,
		'action' : action
		}))

	rsp = requests.post(modify_priv_url, json=rq).json()
	resp = json.loads(decrypt(rsp['secret'], rsp['tag'], rsp['nonce']))

	print(resp['status'])

def modify_rights():
	target_id = get_device_id()

	modifications = []

	services = get_services()

	while True:
		service_id = get_service_id()

		if service_id == '-1':
			break

		while True:
			action = input('Add or remove access (a/r) ? ')

			if action == 'a' or action == 'r':
				break

		modifications.append({'id' : service_id, 'action' : action})

	rq = {}
	rq['secret'], rq['tag'], rq['nonce'] = encrypt(json.dumps({
		'device_id' : device_id,
		'target_id' : target_id,
		'modifications' : modifications
		}))

	rsp = requests.post(modify_rights_url, json=rq).json()
	resp = json.loads(decrypt(rsp['secret'], rsp['tag'], rsp['nonce']))

	print(resp['status'])

def fetch_data():
	serv_id = get_service_id()

	if serv_id == '-1':
		return None
	
	rq = {}
	rq['secret'], rq['tag'], rq['nonce'] = encrypt(json.dumps({
		'service_id' : serv_id,
		'device_id' : device_id
		}))

	rsp = requests.post(fetch_token_url, json=rq).json()
	resp = json.loads(decrypt(rsp['secret'], rsp['tag'], rsp['nonce']))

	if resp['status'] != 'success':
		print(resp['status'])
		return None

	token = resp
	data_url = token['rsrc_url']

	print('Token source %s' % (token['source']))

	resp = requests.post(data_url, json=token)

	if resp.text == 'Invalid token': # cached token might have expired in the time between fetching token and sending data request
		rsp = requests.post(fetch_token_url, json=rq).json()
		resp = json.loads(decrypt(rsp['secret'], rsp['tag'], rsp['nonce']))

		if resp['status'] != 'success':
			print(resp['status'])
			return None

		token = resp
		data_url = token['rsrc_url']
	
		print('Token source %s' % (token['source']))

	resp = requests.post(data_url, json=token)

	if resp.text == 'Invalid token': # second failure
		print('Server providing only expired token. Report.')
		return None

	print('Data to be processed : ', resp.text)
	
	return None

if __name__ == '__main__':
	boot()

