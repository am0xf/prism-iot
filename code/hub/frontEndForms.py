from flask_wtf import FlaskForm
from wtforms import StringField, TextField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Length, EqualTo

class LoginForm(FlaskForm):
    # password = PasswordField('Your password', [DataRequired()])
    password = PasswordField('Your password', [DataRequired(),Length(min=8, max=16, message=('Length should be between 8 and 16 chars'))])
    submit = SubmitField('Login')
    
class ChangePassword(FlaskForm):
    password = PasswordField('Your password', [DataRequired(),Length(min=8, max=16, message=('Length should be between 8 and 16 chars')), EqualTo('confirm', message='Passwords entered are different')])
    confirm = PasswordField('Renter your password')
    submit = SubmitField('Set Password')