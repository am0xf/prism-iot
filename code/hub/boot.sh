#!/bin/bash

clear

_term() {
	echo "Shutting down"
	echo $MEMBER_PID $SIGNER_PID $CACHE_PID

	kill -TERM $CACHE_PID $SIGNER_PID $MEMBER_PID
}

trap _term SIGTERM

first_boot=0

if ! [ -d 'logs/' ] # first boot
then
	mkdir logs
	./init-nodes.sh
	MEMBER_PID=$?
	echo $MEMBER_PID
	first_boot=1
fi

# start nodes

ip='127.0.0.1' # always local address so that blockchain is not accessible to any but token cache server

api="net,web3,eth,personal,miner,admin"
mode="full"
net_id=12345
gaslimit=4503599627370495
gasprice=0

signer=$(gawk -F':' '/^Public/ {print $2}' signer)

member=$(gawk -F':' '/^Public/ {print $2}' member)
enode_m=$(cat member-enode)

pswd="acc-pass"

# start member node

if [ $first_boot -eq 0 ] # not first boot, so member node wasn't started in init-nodes.sh
then
	port=54321
	data_dir=./node-m

	geth --nodiscover --allow-insecure-unlock --http --http.addr 0.0.0.0 --http.port 8545 --http.vhosts="*" --port $port --datadir $data_dir --syncmode $mode --networkid $net_id &>> logs/member &
	#geth --nodiscover --port $port --datadir $data_dir --syncmode $mode --networkid $net_id &>> logs/member &
	#geth --nodiscover --datadir $data_dir --syncmode $mode --networkid $net_id &>> logs/member &

	MEMBER_PID=$!
	echo "Member started $MEMBER_PID"
fi

# start signer node

port=54322
data_dir=./node-s

geth --nodiscover --port $port --datadir $data_dir --unlock $signer --password $pswd --syncmode $mode --networkid $net_id --miner.gasprice $gasprice --mine &>> logs/signer &
#geth --nodiscover --datadir $data_dir --unlock $signer --password $pswd --syncmode $mode --networkid $net_id --miner.gasprice $gasprice --mine &>> logs/signer &

SIGNER_PID=$!
echo "Signer started $SIGNER_PID"

# wait for geth ipc to start
while ! [ -e $data_dir/geth.ipc ]
do
	:;
done

echo -n "Peered : "

geth attach $data_dir/geth.ipc --exec "admin.addPeer(\"$enode_m\")"

[ $first_boot -eq 1 ]  && ./deploy-contract.py

./token-cache-server.py &>> logs/token-cache-server &

CACHE_PID=$!
echo "Server started $CACHE_PID"

wait

