#!/usr/bin/env python3

from web3 import Web3, IPCProvider
from web3.middleware import geth_poa_middleware

import json

from Crypto.Random import get_random_bytes

# hub id
hub_id = open('hub-id').read().strip()

# node address

node_data_dir = 'node-m/'
node_url = node_data_dir + 'geth.ipc'

# web3 setup
w3 = Web3(IPCProvider(node_url))

# inject geth_poa middleware to innermost layer
w3.middleware_onion.inject(geth_poa_middleware, layer=0)

# unlock accounts

# get wallet details
lines = open('member').readlines()

for line in lines:
	line = line.strip()

	# address
	if line.startswith('Public address'):
		wallet_addr = line.split(':')[1].strip()
		break
	
wallet_pass = open('acc-pass').read().strip()

unlock_duration = 60

# unlock account on node
w3.geth.personal.unlockAccount(wallet_addr, wallet_pass, unlock_duration)

# set default account
w3.eth.defaultAccount = wallet_addr

# get the contract json
js = json.loads(open('access-rights.json').read().strip())

# abi, bytecode

abi = js['abi']
bytecode = js['bytecode']

# deploy

contract = w3.eth.contract(abi=abi, bytecode=bytecode)
tx_hash = contract.constructor(int(hub_id, 16)).transact()
tx_recpt = w3.eth.waitForTransactionReceipt(tx_hash)
print('Contract deployed')

# write contract address to file
open('contract-addr', 'w').write(tx_recpt.contractAddress+'\n')

