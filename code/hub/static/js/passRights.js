var toSend = {
    'target_id': null,
    'device_id': null,
    'modifications': null
}
var modifications = []
console.log('testing')

var submit_button = document.getElementById('submit_button')
submit_button.addEventListener("click", function () {
    var device_id = document.getElementById('device_id').textContent
    var hub_id = document.getElementById('submit_button').value
    toSend['device_id'] = hub_id
    toSend['target_id'] = device_id
    ids = document.getElementsByClassName('ids')
    checks = document.getElementsByClassName('checks')
    var i = 0;
    var actVal = null;
    modifications = []
    for (i = 0; i < ids.length; i++) {
        if (checks[i].checked)
            actVal = 'a'
        else actVal = 'r'
        var str = ids[i].textContent
        modifications.push({
            id: str,
            action : actVal
        })
    }
    toSend['modifications'] = modifications
    console.log(JSON.stringify(toSend))
    fetch('/changeservices', {
            method: 'POST', // or 'PUT'
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(toSend),
        })
        .then(function (response){
            if(response.ok)
                console.log('Data posted successfully!')
            else
                return Promise.reject(response)
        })
        .catch((error) => {
            console.error('Error:', error);
        });
})