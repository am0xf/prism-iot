#!/usr/bin/env python3

from flask import Flask, request, jsonify, render_template, redirect, url_for
from frontEndForms import LoginForm, ChangePassword

from web3 import Web3, IPCProvider
from datetime import datetime
import requests
import json

import shelve

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

import signal
import sys

app = Flask('token-caching-server')

app.config['SECRET_KEY']='shakalakaboomboom'

logged_in = False

# cached_tokens structure
# {
#	<service_id : str> : {
#		'service_id' : <str>,
#		'expiry' : <datetimestamp str>,
#		'string' : <hex str>
#		},
#	...}
cached_tokens = {}

# registered_devices structure
# {
#	<device_id : hex_str> : <device_name : str>,
#	...}
registered_devices = shelve.open('registered-devices')

# services structure
# {
#	<service_id : str> : {
#		'name' : <str>,
#		'auth_url' : <str>,
#		'rsrc_url' : <str>
#		},
#	...}
services = shelve.open('services')

# passwords file
passwords = shelve.open('passwords')

# HUB access interface
hub_addr = '127.0.0.1'
hub_port = 12345
hub_id = open('hub-id').read().strip()

# node address
node_data_dir = 'node-m/'
node_url = node_data_dir + 'geth.ipc'

# web3 setup
w3 = Web3(IPCProvider(node_url))

# contract communication setup
contract_addr = open('contract-addr').read().strip()
contract_abi = json.loads(open('access-rights.json').read().strip())['abi']
contract = w3.eth.contract(contract_addr, abi=contract_abi)

# get wallet details
lines = open('member').readlines()

for line in lines:
	line = line.strip()

	# address
	if line.startswith('Public address'):
		wallet_addr = line.split(':')[1].strip()

	# path to primary key
	if line.startswith('Path of'):
		wallet_pk_path = line.split(':')[1].strip()

# wallet password
wallet_pass = open('acc-pass').read().strip()

# wallet primary key
# So that we don't have to unlock accounts on the nodes and passwords never hit the network.
# We sign the transactions and send them manually
wallet_pk = w3.eth.account.decrypt(open(wallet_pk_path).read().strip(), wallet_pass)

# close shelve files
def sigterm_handler(signum, handler):
	services.close()
	registered_devices.close()
	passwords.close()

	sys.exit(0)

signal.signal(signal.SIGTERM, sigterm_handler)

def decrypt(ciphertext, tag, nonce):
	ciphertext = bytes.fromhex(ciphertext)
	tag = bytes.fromhex(tag)
	nonce = bytes.fromhex(nonce)

	cipher_aes_dec = AES.new(passwords['encryption'].encode('utf-8'), AES.MODE_EAX, nonce)

	return cipher_aes_dec.decrypt_and_verify(ciphertext, tag).decode('utf-8')

def encrypt(plaintext):
	plaintext = plaintext.encode('utf-8')

	cipher_aes_enc = AES.new(passwords['encryption'].encode('utf-8'), AES.MODE_EAX)

	ciphertext, tag = cipher_aes_enc.encrypt_and_digest(plaintext)

	return (ciphertext.hex(), tag.hex(), cipher_aes_enc.nonce.hex())

# backend routes ==========================================================================================================================

@app.route('/getDevices', methods=['GET'])
def get_devices():
	resp = {}
	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps(dict(registered_devices)))

	return jsonify(resp)

@app.route('/getServices', methods=['GET'])
def get_services():
	resp = {}
	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps(dict(services)))

	return jsonify(resp)

@app.route('/register', methods=['POST'])
def register():
	rqst = request.get_json()

	resp = {'status' : ''}

	try:
		dev_id = decrypt(rqst['secret'], rqst['tag'], rqst['nonce'])

		if dev_id in registered_devices: # device already registered
			resp['status'] = 'Already registered'

		else:
			registered_devices[dev_id] = rqst['device_name']
			resp['status'] = 'Registered'

	except:
		# incorrect password
		resp['status'] = 'Incorrect password'

	return jsonify(resp)

@app.route('/removeDev', methods=['POST'])
def remove_dev():
	rq = request.get_json()
	rqst = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))

	device_id, target_id = rqst['device_id'], rqst['target_id']
	device_id_i = int(device_id, 16)

	# check if devices is registered
	if target_id not in registered_devices:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Target device not registerd to use token caching'}))

		return jsonify(resp)

	# check if devices is privileged
	rsp = contract.functions.devices(device_id_i).call()

	if rsp == False:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device not privileged'}))

		return jsonify(resp)

	del registered_devices[target_id]

	resp = {}
	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device removed'}))

	return jsonify(resp)

@app.route('/checkPriv', methods=['POST'])
def check_priv():
	rq = request.get_json()
	rqst = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))

	device_id = rqst['device_id']
	device_id_i = int(rqst['device_id'], 16)

	# check if device is registered
	if device_id not in registered_devices:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device not registered to use token caching'}))

		return jsonify(resp)

	rsp = contract.functions.devices(device_id_i).call() # Returns the boolean value privileged in the struct in the mapping. Check the contract

	resp = {}
	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : ('P' if rsp else 'Not p') + 'rivileged'}))

	return jsonify(resp)

@app.route('/modifyPriv', methods=['POST'])
def modify_priv():
	rq = request.get_json()
	rqst = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))

	device_id, target_id, action = rqst['device_id'], rqst['target_id'], rqst['action']
	device_id_i, target_id_i = int(rqst['device_id'], 16), int(rqst['target_id'], 16)

	# check if devices is registered
	if target_id not in registered_devices:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Target device not registered to use token caching'}))

		return jsonify(resp)

	# check if devices is privileged
	rsp = contract.functions.devices(device_id_i).call()

	if rsp == False:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device not privileged'}))

		return jsonify(resp)

	txn_data = {
			'from' : wallet_addr,
			# which one out of the following 2 works keeps changing with every update of geth
			'nonce' : w3.eth.getTransactionCount(wallet_addr, 'pending') 
			#'nonce' : w3.eth.getTransactionCount(wallet_addr)
			}

	txn = contract.functions.modify_privilege(device_id_i, target_id_i, True if action == 'a' else False).buildTransaction(txn_data)

	sgn_txn = w3.eth.account.signTransaction(txn, wallet_pk)
	tx_hash = w3.eth.sendRawTransaction(sgn_txn.rawTransaction)
	recpt = w3.eth.waitForTransactionReceipt(tx_hash)

	rsp = bool(int(recpt['logs'][0]['data'], 16))

	resp = {}
	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Modifying privilege ' + ('' if rsp else 'un') + 'successful'}))

	return jsonify(resp)

@app.route('/modifyRights', methods=['POST'])
def modify_rights():
	rq = request.get_json()
	rqst = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))

	target_id, device_id = rqst['target_id'], rqst['device_id']
	target_id_i, device_id_i = int(rqst['target_id'], 16), int(rqst['device_id'], 16)

	# check if devices is registered
	if device_id not in registered_devices:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device not registered to use token caching'}))

		return jsonify(resp)

	# check if devices is privileged
	rsp = contract.functions.devices(device_id_i).call()

	if rsp == False:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device not privileged'}))

		return jsonify(resp)

	service_ids = []
	modifications = []

	# rqst['modifications'] : [{'id' : <service id : str> : 'action' : <'a' or 'r'>}, ...]

	for entry in rqst['modifications']:
		service_ids.append(int(entry['id'], 16))
		modifications.append(True if entry['action'] == 'a' else False)

	txn_data = {
			'from' : wallet_addr,
			# which one out of the following 2 works keeps changing with every update of geth
			'nonce' : w3.eth.getTransactionCount(wallet_addr, 'pending')
			#'nonce' : w3.eth.getTransactionCount(wallet_addr)
			}

	txn = contract.functions.modify_access_rights(device_id_i, target_id_i, service_ids, modifications).buildTransaction(txn_data)

	sgn_txn = w3.eth.account.signTransaction(txn, wallet_pk)
	tx_hash = w3.eth.sendRawTransaction(sgn_txn.rawTransaction)
	recpt = w3.eth.waitForTransactionReceipt(tx_hash)

	rsp = bool(int(recpt['logs'][0]['data'], 16))

	resp = {}
	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Modifying rights ' + ('' if rsp else 'un') + 'successful'}))

	return jsonify(resp)

@app.route('/fetchToken', methods=['POST'])
def fetch_token():
	rq = request.get_json()
	rqst = json.loads(decrypt(rq['secret'], rq['tag'], rq['nonce']))

	service_id, device_id = rqst['service_id'], rqst['device_id']
	service_id_i, device_id_i = int(rqst['service_id'], 16), int(rqst['device_id'], 16)

	# check if device is registered
	if device_id not in registered_devices:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Device not registered to use token caching'}))

		return jsonify(resp)

	# check if service is supported
	if service_id not in services:
		resp = {}
		resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps({'status' : 'Service not supported for token caching'}))

		return jsonify(resp)

	rsp = contract.functions.access_permitted(device_id_i, service_id_i).call()

	source = 'None'

	if rsp:
		auth_url = services[service_id]['auth_url']

		if service_id in cached_tokens and datetime.fromtimestamp(int(cached_tokens[service_id]['expiry'])) > datetime.now(): # Please note that there might be timezone problems
			token = cached_tokens[service_id]
			source = 'cache'

		else:
			token = requests.post(auth_url, data=service_id).json()
			source = 'auth_server'

		cached_tokens[service_id] = token

		token['status'] = 'success'

	else:
		token = {'status' : 'Device not permitted to access token caching service'}

	resp = {}

	token['rsrc_url'] = services[service_id]['rsrc_url']
	token['source'] = source

	resp['secret'], resp['tag'], resp['nonce'] = encrypt(json.dumps(token))

	return jsonify(resp)

# front-end routes ==========================================================================================================================
@app.route('/', methods=['GET', 'POST'])
def index():
    global logged_in
    logged_in = False
    form = LoginForm()
    if (form.validate_on_submit() and form.password.data==passwords['login']):
        logged_in = True
        return redirect(url_for('landing'))
    return render_template('index.html', form=form)

@app.route('/landing', methods=['GET'])
def landing():
    global logged_in
    if(logged_in == False):
    	return redirect(url_for('index'))
    return render_template('landing.html', password=passwords['encryption'])

@app.route('/logout', methods=['GET'])
def logout():
    global logged_in
    logged_in=False
    return redirect(url_for('index'))

@app.route('/registerPass', methods=['GET', 'POST'])
def registerPass():
	global logged_in, passwords
	if(logged_in != True):
		return redirect(url_for('index'))
	form = ChangePassword()
	if(form.validate_on_submit() and request.method=='POST'):
		if(form.password.data==form.confirm.data):
			passwords['encryption']=form.password.data
		return redirect(url_for('index'))
	return render_template('registerPass.html', form=form)

@app.route('/devicelist', methods=['GET'])
def devicelist():
    global logged_in
    if(logged_in != True):
    	return redirect(url_for('index'))
    return render_template('listOfDevices.html', devices = registered_devices)

@app.route('/changeprivilege', methods=['GET', 'POST'])
def changeprivilege():
	global logged_in
	if(logged_in != True):
		return redirect(url_for('index'))
	if(request.method=='POST'):
		req_data = request.get_json()
		modifications=req_data['modifications']
		for mod in modifications:
			mod['device_id']=hub_id
			enc_data = encrypt(json.dumps(mod))
			post_val=requests.post('http://'+str(hub_addr)+':'+str(hub_port)+'/modifyPriv',json = {'secret':enc_data[0], 'tag':enc_data[1], 'nonce':enc_data[2]})
	priv_devices = {}
	for dev in registered_devices:
		temp={'device_id':dev}
		enc_data = encrypt(json.dumps(temp))
		resp = requests.post('http://'+str(hub_addr)+':'+str(hub_port)+'/checkPriv', json = {'secret':enc_data[0], 'tag':enc_data[1], 'nonce':enc_data[2]})
		if resp.status_code != 200:
			print("Error:", resp.status_code)
		data = resp.json()
		data = json.loads(decrypt(data['secret'], data['tag'], data['nonce']))
		priv_devices[dev]={'name':registered_devices[dev], 'a':False}
		if(data['status']=='Privileged'):
			priv_devices[dev]['a']=True
	return render_template('privilegedDevices.html', priv_devices = priv_devices)

@app.route('/changeservices', methods=['GET','POST'])
def changeservices():
	global logged_in
	if(logged_in != True):
		return redirect(url_for('index'))
	if(request.method=='POST'):
		req_data = request.get_json()
		req_data['device_id']=hub_id
		enc_data = encrypt(json.dumps(req_data))
		resp = requests.post('http://'+str(hub_addr)+':'+str(hub_port)+'/modifyRights', json = {'secret':enc_data[0], 'tag':enc_data[1], 'nonce':enc_data[2]})
		res = resp.json()
		res = json.loads(decrypt(res['secret'], res['tag'], res['nonce']))
	if(request.args):
		device_id = request.args.get('device_id')
		device_name = request.args.get('device_name')
		srv={}
		for sid in services:
			srv[sid]={'id':sid, 'name':services[sid], 'a':False}
			if contract.functions.access_permitted(int(device_id, 16), int(sid, 16)).call():
				srv[sid]={'id':sid, 'name':services[sid], 'a':True}
		return render_template('deviceAllowedServices.html', device_id=device_id, device_name=device_name, services=srv)
	return redirect('/devicelist')

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=hub_port)

