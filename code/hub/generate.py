#!/usr/bin/env python3

from Crypto.Random import get_random_bytes

import shelve

# generate account password
active_acc_pass = get_random_bytes(64).hex()
open('acc-pass', 'w').write(active_acc_pass)

print('Account password generated')

# generate hub id
hub_id = get_random_bytes(8).hex() # generate a hub id
open('hub-id', 'w').write(hub_id)

print('Hub ID generated')

# register hub
registered_devices = shelve.open('registered-devices')
registered_devices[hub_id] = 'Hub'
registered_devices.close()

print('Hub registered')

# generate encryption password
passwords = shelve.open('passwords')
passwords['encryption'] = get_random_bytes(8).hex()
passwords['login'] = '123456789'
passwords.close()

print('Encryption password generated')
print('Default admin panel password is 123456789')

