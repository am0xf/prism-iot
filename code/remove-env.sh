#!/bin/bash

sudo docker rm d{1..3}
rm -rf ./device/d{1..3}

sudo docker rm servers
rm -rf servers/logs

sudo docker rm hub
rm -r hub/{.puppeth/chain,signer,member,contract-addr,member-enode,acc-pass,passwords,hub-id,registered-devices,node-m/,node-s/,logs/}

docker network rm test

