#!/bin/bash

clear

docker network create test

cd ./device
IMG_NAME=device_image
sudo docker build -t $IMG_NAME .

CONT_NAME="d1"
mkdir $CONT_NAME
cp device.py $CONT_NAME/device.py
sudo docker create --name $CONT_NAME --net="test" -v $(pwd)/$CONT_NAME:/home/device -it $IMG_NAME

CONT_NAME="d2"
mkdir $CONT_NAME
cp device.py $CONT_NAME/device.py
sudo docker create --name $CONT_NAME --net="test" -v $(pwd)/$CONT_NAME:/home/device -it $IMG_NAME

CONT_NAME="d3"
mkdir $CONT_NAME
cp device.py $CONT_NAME/device.py
sudo docker create --name $CONT_NAME --net="test" -v $(pwd)/$CONT_NAME:/home/device -it $IMG_NAME

cd ../servers
IMG_NAME=servers_image
sudo docker build -t $IMG_NAME .
CONT_NAME=servers
sudo docker create --name $CONT_NAME --net="test" -v $(pwd):/home/servers $IMG_NAME

cd ../hub
IMG_NAME=hub_image
sudo docker build -t $IMG_NAME .
CONT_NAME=hub
sudo docker create --name $CONT_NAME --net="test" -v $(pwd):/home/hub -p 12345:12345 $IMG_NAME

ret=$(docker-compose -v)

if ! [ $? -eq 0 ]
then
	echo "Installing docker-compose"

	sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
fi

cd ../epirus-free

sudo NODE_ENDPOINT=http://hub.test:8545 docker-compose pull

