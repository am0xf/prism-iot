#!/bin/bash

./build-env.sh

sudo docker start servers
sudo docker start hub

cd ./epirus-free
sudo NODE_ENDPOINT=http://hub.test:8545 docker-compose up &> log &

echo "Start devices manually"

