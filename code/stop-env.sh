#!/bin/bash

sudo docker stop -t 30 d1 d2 d3

sudo docker stop -t 30 servers

sudo docker stop -t 30 hub

cd ./epirus-free
sudo docker-compose down

cd ..
./remove-env.sh

